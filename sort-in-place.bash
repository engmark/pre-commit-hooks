#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

# Default to POSIX locale for reproducibility
default_locale='POSIX'
LC_COLLATE="${default_locale}"

if [[ "${1::9}" == '--locale=' ]]; then
    LC_COLLATE="${1:9}"
    shift
fi
export LC_COLLATE

if (($# == 0)); then
    escaped_command="$(printf '%q' "$0")"
    # shellcheck disable=SC1111
    cat >&2 << EOF
Usage:

    ${escaped_command} [--locale=LOCALE] FILE…

Options

    --locale=LOCALE

        Set LOCALE to a locale to sort according to that locale.
        Default: “${default_locale}”

Examples:

    ${escaped_command} .gitignore

        Sort .gitignore according to ${default_locale} locale.

    ${escaped_command} --locale=en_NZ.UTF-8 .gitignore

        Sort .gitignore according to New Zealand English locale.
EOF
    exit 2
fi

printf 'Using locale “%q”\n' "${LC_COLLATE}" >&2

for path; do
    printf 'Sorting file “%q”\n' "${path}" >&2
    sort --output="${path}" --unique -- "${path}"
done
