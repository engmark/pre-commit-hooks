#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

if (($# == 0)); then
    escaped_command="$(printf '%q' "$0")"
    cat >&2 << EOF
Usage:

    ${escaped_command} FILE…

Examples:

    ${escaped_command} my.json

        Sort my.json keys.
EOF
    exit 2
fi

for path; do
    printf 'Sorting keys in JSON file “%q”\n' "${path}" >&2
    jq --sort-keys . "${path}" | sponge "${path}"
done
