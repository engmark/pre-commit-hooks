# sort pre-commit hooks

Keep files sorted with [`pre-commit`](https://pre-commit.com/).

Replace `REVISION` with the relevant commit ID in the below.

## Sort files alphabetically

Prerequisites: GNU `sort`

### Simple use

To sort CSV and .gitignore files using the POSIX locale:

<!-- prettier-ignore-start -->
```yaml
repos:
  - repo: https://gitlab.com/engmark/sort-hook
    rev: REVISION
    hooks:
      - id: sort
        types: [file]
        types_or: [csv, gitignore]
```
<!-- prettier-ignore-end -->

### Use with a specific locale

Same as the above, sorted according to the `en_US.UTF-8` locale, for a more
human-readable result:

<!-- prettier-ignore-start -->
```yaml
repos:
  - repo: https://gitlab.com/engmark/sort-hook
    rev: REVISION
    hooks:
      - id: sort
        entry: ./sort-in-place.bash --locale=en_US.UTF-8
        types: [file]
        types_or: [csv, gitignore]
```
<!-- prettier-ignore-end -->

### Stand-alone use

```bash
./sort-in-place.bash FILE…
```

## Sort JSON keys recursively by Unicode codepoint order

Prerequisites: [`jq`](https://jqlang.github.io/jq/) and
[`sponge`](https://joeyh.name/code/moreutils/)

### Use

<!-- prettier-ignore-start -->
```yaml
repos:
  - repo: https://gitlab.com/engmark/sort-hook
    rev: REVISION
    hooks:
      - id: sort-json-keys
        types: [json]
```
<!-- prettier-ignore-end -->

### Stand-alone use

```bash
./sort-json-keys-in-place.bash FILE…
```
