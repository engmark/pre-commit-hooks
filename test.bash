#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

sorted_non_posix_text_file="${script_dir}/tests/en_NZ.UTF-8-sorted.txt"
echo "${sorted_non_posix_text_file} should already be sorted…"
"${script_dir}/sort-in-place.bash" --locale=en_NZ.UTF-8 "${sorted_non_posix_text_file}"
git diff --exit-code "${sorted_non_posix_text_file}"

non_posix_reversed_file="${script_dir}/tests/en_NZ.UTF-8-reversed.txt"
echo "${non_posix_reversed_file} should be sorted…"
"${script_dir}/sort-in-place.bash" --locale=en_NZ.UTF-8 "${non_posix_reversed_file}"

echo 'The files should now be identical…'
diff "${sorted_non_posix_text_file}" "${non_posix_reversed_file}"
git checkout "${non_posix_reversed_file}"

posix_sorted_text_file="${script_dir}/tests/POSIX-sorted.txt"
echo "${posix_sorted_text_file} should already be sorted…"
pre-commit try-repo --files="${posix_sorted_text_file}" --verbose . sort

posix_reversed_text_file="${script_dir}/tests/POSIX-reversed.txt"
echo "${posix_reversed_text_file} should be sorted…"
if pre-commit try-repo --files="${posix_reversed_text_file}" --verbose . sort; then
    exit 1
fi

echo 'The files should now be identical…'
diff "${posix_sorted_text_file}" "${posix_reversed_text_file}"
git checkout "${posix_reversed_text_file}"

sorted_json_file="${script_dir}/tests/sorted.json"
echo "${sorted_json_file} should already be sorted…"
pre-commit try-repo --files="${sorted_json_file}" --verbose . sort-json-keys

reversed_json_file="${script_dir}/tests/reversed.json"
echo "${reversed_json_file} should be sorted…"
if pre-commit try-repo --files="${reversed_json_file}" --verbose . sort-json-keys; then
    exit 1
fi

echo 'The files should now be identical…'
diff "${sorted_json_file}" "${reversed_json_file}"
git checkout "${reversed_json_file}"
